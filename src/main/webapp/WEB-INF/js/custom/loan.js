$(function () {
    // Loan apply button handler
    $('#slider-take-loan').click(function () {
        $('#apply_for_loan').submit();
    });

    $('[rel=popover]').popover({
            live: true,
            html: true,
            trigger: 'click',
            content: function () {
                var loanId = $(this).attr('loan-id');
                var lExt = $("#loan-extensions-" + loanId);
                if (!lExt.is(':empty')) {
                    return lExt.html();
                }
                return "No history available";
            }}
    );

    // Loan pay button clicked
    window.payLoan = function (loanId) {
        $.post("/loan/pay", {
                loanId: loanId
            },
            function (data) {
                location.reload();
            });
    };

    // Loan extend button clicked
    window.extendLoan = function (loanId) {
        const loanExtensionDays = 7; // Just for the purpose of homework, we will extend by one week
        $.post("/loan/extend", {
                loanId: loanId,
                loanExtensionDays: loanExtensionDays
            },
            function (data) {
                location.reload();
            });
    };
});