package com.finance.web.controller.auth;

import com.finance.domain.User;
import com.finance.service.user.UserService;
import com.finance.session.SecureSessionAccessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;

/**
 * Controller for authentication and authorization + registration of the user
 *
 * @author vitaly.derkach
 */
@Controller
public class AuthController {

    // Pages
    public static final String LOGIN_PAGE = "auth/login";
    public static String REGISTER_PAGE = "auth/register";
    public static String DENIED_PAGE = "auth/denied";

    // Requests
    public static final String AUTH_LOGIN = "/auth/login";
    public static final String AUTH_LOGOUT = "/auth/logout";
    public static final String AUTH_ERROR = "/auth/error";
    public static final String AUTH_REGISTER = "/auth/register";
    public static final String AUTH_REGISTER1 = "/auth/register";
    public static final String AUTH_DENIED = "/auth/denied";

    @Autowired
    @Qualifier("userService")
    private UserService userService;

    @Autowired
    private SecureSessionAccessor sessionAccessor;

    @RequestMapping(value = AUTH_LOGIN)
    public String login(@ModelAttribute("user") User user) {
        return LOGIN_PAGE;
    }

    @RequestMapping(value = AUTH_REGISTER, method = RequestMethod.GET)
    public String register(@ModelAttribute("user") User user) {
        return REGISTER_PAGE;
    }

    @RequestMapping(value = AUTH_REGISTER1, method = RequestMethod.POST)
    public String register(@ModelAttribute("user") @Valid User user, BindingResult result) {
        if (result.hasErrors()) {
            // Model fill be filled with error data
            return REGISTER_PAGE;
        }
        // TODO: validate result
        userService.createUser(user);

        return "redirect:/";
    }

    @RequestMapping(value = AUTH_DENIED)
    public String denied() {
        return DENIED_PAGE;
    }

    @RequestMapping(value = AUTH_LOGOUT)
    public String logout() {
        sessionAccessor.logoutUser();
        return "redirect:/";
    }

    @RequestMapping(value = AUTH_ERROR)
    public ModelAndView error() {
        ModelAndView model = new ModelAndView();
        model.setViewName(LOGIN_PAGE);
        model.addObject("error", 1);

        return model;
    }

}
