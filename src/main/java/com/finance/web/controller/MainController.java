package com.finance.web.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author vitaly.derkach
 */
@Controller
public class MainController {

    public static final String MAIN_PAGE = "main/main";

    @RequestMapping("/")
    public String index() {
        return MAIN_PAGE;
    }

}
