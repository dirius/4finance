package com.finance.web.controller.slider;

import com.finance.domain.loan.Loan;
import com.finance.domain.loan.LoanExtension;
import com.finance.domain.loan.LoanStatus;
import com.finance.service.loan.LoanService;
import com.finance.service.user.UserService;
import com.finance.service.user.data.UserData;
import com.finance.session.SecureSessionAccessor;
import com.finance.web.slider.data.LoanDataFactory;
import com.finance.web.slider.data.LoanJSONData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * @author vitaly.derkach
 */
@Controller
public class LoanController {

    // Requests
    public static final String LOAN_APPLY = "/loan/apply";
    public static final String LOAN_EXTEND = "/loan/extend";
    public static final String LOAN_PAY = "/loan/pay";
    public static final String SLIDER_GET_DATA = "/slider/getData";
    public static final String REQUEST_MY_LOANS = "/loans";

    // Views
    public static final String VIEW_REJECTED_LOAN = "main/loan-reject";
    private static final String VIEW_MY_LOANS = "main/loans";

    @Autowired
    private SecureSessionAccessor sessionAccessor;

    @Autowired
    private LoanService loanService;

    @Autowired
    private UserService userService;


    @RequestMapping(value = SLIDER_GET_DATA, method = RequestMethod.POST, consumes = "application/json")
    public
    @ResponseBody
    LoanJSONData sliderStopAJAX(@RequestBody Map<String, String> sliderData) {
        return LoanDataFactory.createLoanDataFromRequestMap(sliderData);

    }

    @RequestMapping(value = LOAN_APPLY, method = RequestMethod.POST)
    public String applyForLoan(HttpServletRequest request) {
        final LoanJSONData loanJSONData = LoanDataFactory.createSliderDataFromRequest(request);
        Loan loan = loanJSONData.getSimpleLoan();

        UserData userData = getCurrentUserData();

        userData = loanService.createLoanForUser(userData, loan);

        final Loan currentLoan = userData.getLatestLoan();
        if (isRejected(currentLoan)) {
            return VIEW_REJECTED_LOAN;
        }

        return "redirect:" + REQUEST_MY_LOANS;
    }

    private boolean isRejected(Loan currentLoan) {
        return LoanStatus.REJECTED.equals(currentLoan.getLoanStatus());
    }

    @RequestMapping(value = LOAN_PAY, method = RequestMethod.POST)
    public String payLoan(@RequestParam("loanId") Long loanId) {

        final Long userId = sessionAccessor.getCurrentUserId();
        loanService.payLoanForUser(loanId, userId);

        return "redirect:" + REQUEST_MY_LOANS;
    }

    @RequestMapping(value = LOAN_EXTEND, method = RequestMethod.POST)
    public String extendLoan(@RequestParam("loanId") Long loanId, @RequestParam("loanExtensionDays") Byte loanExtensionDays) {

        loanService.extendLoan(loanId, loanExtensionDays);

        return "redirect:" + REQUEST_MY_LOANS;
    }


    @RequestMapping(REQUEST_MY_LOANS)
    public ModelAndView loans() {
        final UserData userData = getCurrentUserData();
        List<Loan> userLoans = userData.getLoans();
        // Sort the loans
        Collections.sort(userLoans);

        ModelAndView model = new ModelAndView();
        model.setViewName(VIEW_MY_LOANS);
        model.addObject("loans", userLoans);

        return model;
    }

    private UserData getCurrentUserData() {
        final Long userId = sessionAccessor.getCurrentUserId();
        return userService.getById(userId);
    }
}
