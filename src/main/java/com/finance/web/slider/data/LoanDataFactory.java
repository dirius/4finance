package com.finance.web.slider.data;

import com.finance.util.CalcUtil;
import com.finance.util.DateUtil;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.Map;

/**
 * @author vitaly.derkach
 */
public class LoanDataFactory {

    private static final Logger log = Logger.getLogger(LoanDataFactory.class);

    public static final String PARAM_AMOUNT = "amount";
    public static final String PARAM_TIME = "time";

    public static LoanJSONData createLoanDataFromRequestMap(Map<String, String> requestMap) {
        try {
            return getSliderDataFromRequestMap(requestMap);
        } catch (NumberFormatException e) {
            return logErrorAndReturnEmptySliderData(e);
        }
    }

    public static LoanJSONData createSliderDataFromRequest(HttpServletRequest request) {
        try {
            return getSliderDataFromRequest(request);
        } catch (NumberFormatException e) {
            return logErrorAndReturnEmptySliderData(e);
        }
    }

    public static LoanJSONData getSliderDataFromRequest(HttpServletRequest request) {
        final Integer loanAmount = Integer.valueOf(request.getParameter(PARAM_AMOUNT));
        final Byte loanDays = Byte.valueOf(request.getParameter(PARAM_TIME));

        return getSliderDataFromRequestMap(loanAmount, loanDays);
    }

    public static LoanJSONData getSliderDataFromRequestMap(Map<String, String> requestMap) {
        final Integer loanAmount = Integer.valueOf(requestMap.get(PARAM_AMOUNT));
        final Byte loanDays = Byte.valueOf(requestMap.get(PARAM_TIME));

        return getSliderDataFromRequestMap(loanAmount, loanDays);
    }


    private static LoanJSONData getSliderDataFromRequestMap(final Integer loanAmount, final Byte loanDays) {
        final Date returnDate = DateUtil.addDaysCurrentTime(loanDays);
        final double loanCommission = calculateCommission(loanAmount, loanDays);
        final double total = loanAmount + loanCommission;


        return new LoanDataBuilder()
                .setAmount(loanAmount)
                .setTime(loanDays)
                .setReturnDate(returnDate)
                .setCommission(loanCommission)
                .setTotal(total)
                .createLoanJSONData();
    }

    private static double calculateCommission(Integer loanAmount, Byte loanDays) {
        // 15% is the commission
        double percentCommission = CalcUtil.getPercentFrom(CalcUtil.PERCENT_15, loanAmount);
        double daysCoefficient = CalcUtil.getRelativeValueFrom(DateUtil.DAYS_IN_MONTH, loanDays);
        return CalcUtil.roundDoubleTo2Decimal(percentCommission * daysCoefficient);
    }

    private static LoanJSONData logErrorAndReturnEmptySliderData(NumberFormatException e) {
        if (log.isDebugEnabled()) {
            log.debug("Could not parse params from JSON for LoanJSONData creation. Returning empty LoanJSONData.", e);
        }
        return new LoanDataBuilder().createLoanJSONData();
    }

}
