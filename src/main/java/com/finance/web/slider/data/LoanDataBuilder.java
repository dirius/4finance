package com.finance.web.slider.data;

import java.util.Date;

public class LoanDataBuilder {
    private Integer amount;
    private Double commission;
    private Double total;
    private Byte time;
    private Date returnDate;

    public LoanDataBuilder setAmount(Integer amount) {
        this.amount = amount;
        return this;
    }

    public LoanDataBuilder setCommission(Double commission) {
        this.commission = commission;
        return this;
    }

    public LoanDataBuilder setTotal(Double total) {
        this.total = total;
        return this;
    }

    public LoanDataBuilder setTime(Byte time) {
        this.time = time;
        return this;
    }

    public LoanDataBuilder setReturnDate(Date returnDate) {
        this.returnDate = returnDate;
        return this;
    }

    public LoanJSONData createLoanJSONData() {
        return new LoanJSONData(amount, commission, total, time, returnDate);
    }
}