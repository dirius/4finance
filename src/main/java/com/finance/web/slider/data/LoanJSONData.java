package com.finance.web.slider.data;

import com.finance.domain.loan.Loan;
import com.finance.util.DateUtil;

import java.util.Date;

/**
 * Slider data bean / used as a JSON object transferred via Ajax for slider information update
 * Contains current user's loan state
 *
 * @author vitaly.derkach
 */
public class LoanJSONData {
    private Integer amount = 0;
    private Double commission = 0.0;
    private Double total = 0.0;
    private Byte time = 0;
    private Date returnDate;


    public LoanJSONData(Integer amount, Double commission, Double total, Byte time, Date returnDate) {
        this.amount = amount;
        this.commission = commission;
        this.total = total;
        this.time = time;
        this.returnDate = returnDate;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public Integer getAmount() {

        return amount;
    }

    public Double getCommission() {
        return commission;
    }

    public void setCommission(Double commission) {
        this.commission = commission;
    }

    public Double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }

    public void setTime(Byte time) {
        this.time = time;
    }

    public void setReturnDate(Date returnDate) {
        this.returnDate = returnDate;
    }

    public byte getTime() {
        return time;
    }

    public String getReturnDateAsFormattedString() {
        return DateUtil.formatDate(returnDate);
    }

    public Date getReturnDate() {
        return returnDate;
    }

    public boolean isValid() {
        return amount != null & time != null;
    }

    public Loan getSimpleLoan() {
        Loan loan = new Loan();
        loan.setAmount(amount);
        loan.setCommission(commission);
        loan.setTotalAmount(total);
        loan.setReturnDate(returnDate);

        return loan;
    }

}
