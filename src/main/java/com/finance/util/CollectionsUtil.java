package com.finance.util;

import java.util.List;

/**
 * @author vitaly.derkach
 */
public class CollectionsUtil {

    public static <T> T getLastElementInList(List<T> list) {
        if (!list.isEmpty()) {
            return list.get(list.size() - 1);
        }
        return null;
    }

}
