package com.finance.util;

import java.math.BigDecimal;

/**
 * @author vitaly.derkach
 */
public class CalcUtil {

    public static final int PERCENT_100 = 100;
    public static final int PERCENT_15 = 15;

    private static final int DECIMAL_PLACES = 3;

    public static double getPercentFrom(double percent, int totalAmount) {
        return ((double) totalAmount / PERCENT_100) * percent;
    }

    public static double getRelativeValueFrom(int relativeTotal, int neededAmount) {
        return ((double) 1 / relativeTotal) * neededAmount;
    }

    public static double roundDoubleTo2Decimal(double rawDouble) {
        BigDecimal bd = new BigDecimal(rawDouble);
        BigDecimal rounded = bd.setScale(DECIMAL_PLACES, BigDecimal.ROUND_HALF_UP);
        return rounded.doubleValue();
    }
}
