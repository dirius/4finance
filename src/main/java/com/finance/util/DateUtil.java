package com.finance.util;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * @author vitaly.derkach
 */
public class DateUtil {

    private static SimpleDateFormat DATE_FORMAT =
            new SimpleDateFormat("dd.MM.yyyy");

    public static final int DAYS_IN_MONTH = 30;
    public static final int DAYS_IN_WEEK = 7;

    public static Date addDaysCurrentTime(Byte days) {
        return addDaysToDate(new Date(), days);
    }

    public static Date addDaysToDate(Date date, Byte days) {
        Calendar now = Calendar.getInstance();
        now.setTime(date);
        now.add(Calendar.DAY_OF_MONTH, days);
        return removeTimeFromDate(now.getTime());
    }

    public static Date removeTimeFromDate(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        return cal.getTime();
    }

    public static String formatDate(Date date) {
        return DATE_FORMAT.format(date);
    }

    public static int getDifferenceInDays(Date startDate, Date endDate) {
        return (int) (Math.abs((startDate.getTime() - endDate.getTime()))
                / (1000 * 60 * 60 * 24)) + 1;
    }

}
