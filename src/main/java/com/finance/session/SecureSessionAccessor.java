package com.finance.session;

import com.finance.service.user.data.UserData;

/**
 * Provides methods to access/store user credentials in session securely
 *
 * @author vitaly.derkach
 */
public interface SecureSessionAccessor {

    @Deprecated
    UserData getCurrentUserFromSession();

    Long getCurrentUserId();

    void logoutUser();

}
