package com.finance.session;

import com.finance.service.user.data.UserData;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

/**
 * @author vitaly.derkach
 */
@Service
public class SecureSessionAccessorImpl implements SecureSessionAccessor {

    @Override
    public UserData getCurrentUserFromSession() {
        Authentication authentication = getContext().getAuthentication();
        return (UserData) authentication.getPrincipal();
    }

    @Override
    public Long getCurrentUserId() {
        UserData userData = getCurrentUserFromSession();
        return userData.getId();
    }

    @Override
    public void logoutUser() {
        getContext().getAuthentication().setAuthenticated(false);
    }

    private SecurityContext getContext() {
        return SecurityContextHolder.getContext();
    }




}
