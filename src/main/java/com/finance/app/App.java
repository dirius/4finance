package com.finance.app;

/**
 * @author vitaly.derkach
 */
public class App {
    public static void main(String[] args) throws Exception {
        new App().start();
        // Start DB Gui
    }

    private JettyServer server;

    public App() {
        server = new JettyServer(8000);
    }

    public void start() throws Exception {
        server.start();
        server.join();
    }
}
