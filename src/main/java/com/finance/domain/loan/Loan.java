package com.finance.domain.loan;

import com.finance.domain.User;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * Loan for which a user can apply for
 *
 * @author vitaly.derkach
 */
@Entity
@Table(name = "loan")
public class Loan implements Serializable, Comparable<Loan> {

    private static final long serialVersionUID = 1L;

    private Long id;
    private Date applicationDate;
    private Date returnDate;
    private Integer amount;
    private Double commission;
    private Double totalAmount;
    private User owner;
    // By default the newly created loan is Active
    private LoanStatus loanStatus = LoanStatus.ACTIVE;
    private LoanRisk loanRisk;
    private Set<LoanExtension> loanExtensions = new HashSet<LoanExtension>();

    @Id
    @GeneratedValue
    @Column(name = "id")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "application_date", nullable = false)
    public Date getApplicationDate() {
        return applicationDate;
    }

    public void setApplicationDate(Date applicationDate) {
        this.applicationDate = applicationDate;
    }

    @Column(name = "return_date", nullable = false)
    public Date getReturnDate() {
        return returnDate;
    }

    public void setReturnDate(Date returnDate) {
        this.returnDate = returnDate;
    }

    @Column(name = "amount")
    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    @Column(name = "commission")
    public Double getCommission() {
        return commission;
    }

    public void setCommission(Double commission) {
        this.commission = commission;
    }

    @Column(name = "total")
    public Double getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(Double totalAmount) {
        this.totalAmount = totalAmount;
    }

    @OneToMany(targetEntity = LoanExtension.class, mappedBy = "loan")
    public Set<LoanExtension> getLoanExtensions() {
        return loanExtensions;
    }

    public void setLoanExtensions(Set<LoanExtension> loanExtensions) {
        this.loanExtensions = loanExtensions;
    }


    @ManyToOne
    @JoinColumn(name = "loan_id")
    public User getOwner() {
        return owner;
    }

    public void setOwner(User owner) {
        this.owner = owner;
    }

    @Enumerated(EnumType.STRING)
    @Column(name = "loan_status")
    public LoanStatus getLoanStatus() {
        return loanStatus;
    }

    public void setLoanStatus(LoanStatus loanStatus) {
        this.loanStatus = loanStatus;
    }

    @OneToOne(fetch = FetchType.LAZY, mappedBy = "loan", cascade = CascadeType.ALL)
    public LoanRisk getLoanRisk() {
        return loanRisk;
    }

    public void setLoanRisk(LoanRisk loanRisk) {
        this.loanRisk = loanRisk;
    }

    @Override
    public int compareTo(Loan o) {
        return -o.getLoanStatus().compareTo(loanStatus);
    }
}
