package com.finance.domain.loan;

import com.finance.service.loan.risk.RiskAnalyzerImpl;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.io.Serializable;

/**
 * A Risk entity is associated with every Loan, which represents
 * the risk probability when the particular loan was granted.
 *
 * @author vitaly.derkach
 */
@Entity
@Table(name = "loan_risk")
public class LoanRisk implements Serializable {

    private Long id;
    private Loan loan;
    // Default value = 100%, which is considered a REJECTED by risk analyzer
    private Double riskCoefficient = 1.0;


    @Id
    @GeneratedValue
    @Column(name = "loan_risk_id")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @OneToOne(fetch = FetchType.LAZY)
    @PrimaryKeyJoinColumn
    public Loan getLoan() {
        return loan;
    }

    public void setLoan(Loan loan) {
        this.loan = loan;
    }

    @Column(name = "risk_coefficient", nullable = false)
    public Double getRiskCoefficient() {
        return riskCoefficient;
    }

    public void setRiskCoefficient(Double riskCoefficient) {
        this.riskCoefficient = riskCoefficient;
    }

    @Transient
    public boolean isAcceptable() {
        return RiskAnalyzerImpl.HIGH_RISK_BOUNDARY > riskCoefficient;
    }
}
