package com.finance.domain.loan;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.io.Serializable;
import java.util.Date;

/**
 * @author vitaly.derkach
 */
@Entity
@Table(name = "loan_extension")
public class LoanExtension implements Serializable, Comparable<LoanExtension> {

    private Long id;
    private Loan loan;
    private Date applicationDate;
    private Date returnDate;

    @Id
    @GeneratedValue
    @Column(name = "loan_extension_id")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @ManyToOne
    @JoinColumn(name = "loan_ext_id")
    public Loan getLoan() {
        return loan;
    }

    public void setLoan(Loan loan) {
        this.loan = loan;
    }
    @Column(name = "application_date", nullable = false)
    public Date getApplicationDate() {
        return applicationDate;
    }

    public void setApplicationDate(Date applicationDate) {
        this.applicationDate = applicationDate;
    }
    @Column(name = "return_date", nullable = false)
    public Date getReturnDate() {
        return returnDate;
    }

    public void setReturnDate(Date returnDate) {
        this.returnDate = returnDate;
    }

    @Transient
    @Override
    public int compareTo(LoanExtension o) {
        return 0;//getId().compareTo(o.getId());
    }
}
