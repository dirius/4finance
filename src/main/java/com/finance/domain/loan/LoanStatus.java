package com.finance.domain.loan;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * Represents the status of the particular loan, issued to a user
 * It can be active, overdue or paid;
 * @author vitaly.derkach
 */
public enum LoanStatus {
    OVERDUE(0),
    ACTIVE(1),
    PAID(2),
    REJECTED(3);

    private int statusId;
    private static Map<Integer, LoanStatus> loanStatusMap;

    static {
        loanStatusMap = new HashMap<Integer, LoanStatus>(values().length);
        loanStatusMap = Collections.unmodifiableMap(initMap(loanStatusMap));
    }

    private static Map<Integer, LoanStatus> initMap(Map<Integer, LoanStatus> map) {
        for (LoanStatus status : LoanStatus.values()) {
            map.put(status.getStatusId(), status);
        }
        return map;
    }

    LoanStatus(int statusId) {
        this.statusId = statusId;
    }

    public LoanStatus getByStatusId(int statusId) {
        return loanStatusMap.get(statusId);
    }

    public int getStatusId() {
        return this.statusId;
    }


}
