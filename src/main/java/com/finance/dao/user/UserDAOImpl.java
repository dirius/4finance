package com.finance.dao.user;

import com.finance.dao.AbstractEntityDAO;
import com.finance.domain.User;
import com.finance.exception.PersistentException;
import org.hibernate.Query;
import org.springframework.stereotype.Repository;


/**
 * @author vitaly.derkach
 */
@Repository
public class UserDAOImpl extends AbstractEntityDAO<User> implements UserDAO {

    @Override
    public void remove(Long id) {
        final User user = getById(id);
        getSession().delete(user);
    }

    @Override
    public User getById(Long id) {
        return (User) getSession().load(User.class, id);
    }

    @Override
    public User getByUsername(String username) {
        Query q = getSession().createQuery("from User where username = :name");
        q.setParameter("name", username);
        try {
            return (User) q.uniqueResult();
        } catch (Exception e) {
            throw new PersistentException(e.getMessage());
        }
    }
}
