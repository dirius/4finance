package com.finance.dao.user;

import com.finance.dao.CommonEntityDAO;
import com.finance.domain.User;

/**
 * @author vitaly.derkach
 */
public interface UserDAO extends CommonEntityDAO<User>{
    User getByUsername(String username);

}
