package com.finance.dao.loan;

import com.finance.dao.AbstractEntityDAO;
import com.finance.domain.loan.LoanRisk;
import com.finance.exception.PersistentException;
import org.springframework.stereotype.Repository;

/**
 * @author vitaly.derkach
 */
@Repository
public class LoanRiskDAOImpl extends AbstractEntityDAO<LoanRisk> implements LoanRiskDAO {

    @Override
    public void remove(Long id) throws PersistentException {
        final LoanRisk loanRisk = getById(id);
        getSession().delete(loanRisk);
    }

    @Override
    public LoanRisk getById(Long id) throws PersistentException {
        return (LoanRisk) getSession().load(LoanRisk.class, id);
    }
}
