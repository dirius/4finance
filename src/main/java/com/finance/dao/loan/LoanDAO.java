package com.finance.dao.loan;

import com.finance.dao.CommonEntityDAO;
import com.finance.domain.loan.Loan;

/**
 * @author vitaly.derkach
 */
public interface LoanDAO extends CommonEntityDAO<Loan> {
}
