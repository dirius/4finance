package com.finance.dao.loan;

import com.finance.dao.AbstractEntityDAO;
import com.finance.domain.User;
import com.finance.domain.loan.Loan;
import com.finance.exception.PersistentException;
import org.springframework.stereotype.Repository;

/**
 * @author vitaly.derkach
 */
@Repository
public class LoanDAOImpl extends AbstractEntityDAO<Loan> implements LoanDAO {

    @Override
    public void remove(Long id) throws PersistentException {
        final Loan loan= getById(id);
        getSession().delete(loan);
    }

    @Override
    public Loan getById(Long id) throws PersistentException {
        return (Loan) getSession().load(Loan.class, id);
    }
}
