package com.finance.dao.loan;

import com.finance.dao.CommonEntityDAO;
import com.finance.domain.loan.LoanExtension;

/**
 * @author vitaly.derkach
 */
public interface LoanExtensionDAO extends CommonEntityDAO<LoanExtension> {
}
