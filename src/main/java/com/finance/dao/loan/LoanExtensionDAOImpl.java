package com.finance.dao.loan;

import com.finance.dao.AbstractEntityDAO;
import com.finance.domain.loan.LoanExtension;
import com.finance.exception.PersistentException;
import org.springframework.stereotype.Repository;

/**
 * @author vitaly.derkach
 */
@Repository
public class LoanExtensionDAOImpl extends AbstractEntityDAO<LoanExtension> implements LoanExtensionDAO {
    @Override
    public void remove(Long id) throws PersistentException {
        final LoanExtension loanExtension = getById(id);
        getSession().delete(loanExtension);
    }

    @Override
    public LoanExtension getById(Long id) throws PersistentException {
        return (LoanExtension) getSession().load(LoanExtension.class, id);
    }
}
