package com.finance.dao.loan;

import com.finance.dao.CommonEntityDAO;
import com.finance.domain.loan.LoanRisk;

/**
 * @author vitaly.derkach
 */
public interface LoanRiskDAO extends CommonEntityDAO<LoanRisk> {
}
