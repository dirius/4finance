package com.finance.dao;

import com.finance.exception.PersistentException;

/**
 * Common interface for accessing application Entities
 *
 * @author vitaly.derkach
 */
public interface CommonEntityDAO<T> {

    Long create(T entity) throws PersistentException;

    void update(T entity) throws PersistentException;

    void remove(Long id) throws PersistentException;

    T getById(Long id) throws PersistentException;

}
