package com.finance.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author vitaly.derkach
 */
public abstract class AbstractEntityDAO<T> implements CommonEntityDAO<T> {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public Long create(T entity) {
        return (Long) sessionFactory.getCurrentSession().save(entity);
    }

    @Override
    public void update(T entity) {
        getSession().update(entity);
    }

    protected Session getSession() {
        return sessionFactory.getCurrentSession();
    }
}
