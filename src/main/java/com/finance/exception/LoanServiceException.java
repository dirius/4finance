package com.finance.exception;

/**
 * Exception in loan service interactions
 * @author vitaly.derkach
 */
public class LoanServiceException extends ServiceException {

    public LoanServiceException(String message) {
        super(message);
    }
}
