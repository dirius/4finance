package com.finance.exception;

/**
 * Base exception to be thrown from the Persistence layer
 *
 * @author vitaly.derkach
 */
public class PersistentException extends RuntimeException {

    private String message;

    public PersistentException(String message) {
        this.message = message;
    }

    @Override
    public String getMessage() {
        return this.message;
    }
}
