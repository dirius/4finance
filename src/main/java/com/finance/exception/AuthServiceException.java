package com.finance.exception;

/**
 * @author vitaly.derkach
 */
public class AuthServiceException extends ServiceException {

    public AuthServiceException(String message) {
        super(message);
    }
}
