package com.finance.service.loan;

import com.finance.domain.loan.Loan;
import com.finance.domain.loan.LoanExtension;
import com.finance.service.user.data.UserData;

/**
 * @author vitaly.derkach
 */
public interface LoanService {

    UserData createLoanForUser(UserData userData, Loan loan);

    Loan getLoanById(Long loanId);

    boolean payLoanForUser(Long loanId, Long userId);

    Loan extendLoan(Long loanId, Byte loanExtensionDays);
}
