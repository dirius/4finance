package com.finance.service.loan.risk;

import com.finance.domain.loan.Loan;
import com.finance.domain.loan.LoanRisk;

/**
 * @author vitaly.derkach
 */
public interface LoanRiskService {

    LoanRisk analyzeAndCreateRisk(Loan loan);
}
