package com.finance.service.loan.risk;

import com.finance.dao.loan.LoanRiskDAO;
import com.finance.domain.loan.Loan;
import com.finance.domain.loan.LoanRisk;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author vitaly.derkach
 */
@Service
public class LoanRiskServiceImpl implements LoanRiskService {

    private RiskAnalyzer riskAnalyzer = new RiskAnalyzerImpl();

    @Autowired
    private LoanRiskDAO riskDAO;

    @Override
    public LoanRisk analyzeAndCreateRisk(Loan loan) {
        final LoanRisk loanRisk = riskAnalyzer.analyze(loan);
        riskDAO.create(loanRisk);
        return loanRisk;
    }
}
