package com.finance.service.loan.risk;

import com.finance.domain.loan.Loan;
import com.finance.domain.loan.LoanRisk;

/**
 * Algorithm for loan-associated risk analyzing
 * @author vitaly.derkach
 */
public interface RiskAnalyzer {

    LoanRisk analyze(Loan loan);

}
