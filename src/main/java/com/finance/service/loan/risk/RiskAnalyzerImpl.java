package com.finance.service.loan.risk;

import com.finance.domain.loan.Loan;
import com.finance.domain.loan.LoanRisk;
import com.finance.util.CalcUtil;
import com.finance.util.DateUtil;

/**
 * Encapsulates risk calculating algorithm, which depends
 * on the ration between the amount of loan taken and the days count.
 * If user already has active loans risk is considered 1.0 which is 100% and loan gets rejected.
 * <p/>
 * Just to make it more interesting. Rejects loans when the amount is really high and the days count is really small,
 * i.e. Ls 300 for 1 day - considered high risk and loan gets rejected.
 * <p/>
 * This is mainly for fun, of course this algorithm can't be applied for the real-life, but for the homework it can
 * show how loans get accepted and rejected based on the amount and the days count chosen.
 * <p/>
 * Boundaries were choose empirically, considered to be the best for the example purpose by the author :)
 *
 * @author vitaly.derkach
 */
public class RiskAnalyzerImpl implements RiskAnalyzer {

    private static final int MAX_DAYS = 30;
    private static final int MAX_AMOUNT = 300;

    private static final int LOWEST_DAILY_PAYMENT_RATIO = MAX_AMOUNT / MAX_DAYS;

    // * Empirically chosen value / Risk is considered TOO HIGH and must be REJECTED if the coefficient value us more that 0.77
    public static double HIGH_RISK_BOUNDARY = 0.77;
    public static double MAX_RISK_BOUNDARY = 1.0;

    @Override
    public LoanRisk analyze(Loan loan) {
        double riskCoefficient = calculateRiskCoefficient(loan);

        LoanRisk loanRisk = new LoanRisk();
        loanRisk.setLoan(loan);
        loanRisk.setRiskCoefficient(riskCoefficient);

        return loanRisk;
    }

    private double calculateRiskCoefficient(Loan loan) {
        if (loan.getOwner().hasActiveLoans()) {
            return MAX_RISK_BOUNDARY;
        }
        int loanDays = DateUtil.getDifferenceInDays(loan.getApplicationDate(), loan.getReturnDate());
        final double relationAmountAndDays = ((double) loan.getAmount() / loanDays);
        return CalcUtil.roundDoubleTo2Decimal((MAX_RISK_BOUNDARY - (LOWEST_DAILY_PAYMENT_RATIO / relationAmountAndDays)));
    }

}
