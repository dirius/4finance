package com.finance.service.loan;

import com.finance.dao.loan.LoanDAO;
import com.finance.dao.loan.LoanExtensionDAO;
import com.finance.dao.user.UserDAO;
import com.finance.domain.User;
import com.finance.domain.loan.Loan;
import com.finance.domain.loan.LoanExtension;
import com.finance.domain.loan.LoanRisk;
import com.finance.domain.loan.LoanStatus;
import com.finance.exception.LoanServiceException;
import com.finance.exception.PersistentException;
import com.finance.service.loan.risk.LoanRiskService;
import com.finance.service.user.data.UserData;
import com.finance.util.DateUtil;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

/**
 * Service for creating and dealing with loans
 *
 * @author vitaly.derkach
 */
@Service
public class LoanServiceImpl implements LoanService {

    private static final Logger log = Logger.getLogger(LoanServiceImpl.class);

    @Autowired
    private UserDAO userDAO;

    @Autowired
    private LoanDAO loanDAO;

    @Autowired
    private LoanExtensionDAO loanExtensionDAO;

    @Autowired
    private LoanRiskService loanRiskService;

    @Transactional
    @Override
    public UserData createLoanForUser(UserData userData, Loan loan) {
        try {
            User user = userDAO.getById(userData.getId());
            loan = updateAndPersistLoan(loan, user);
            // Add loan to a user and persist
            user.getLoans().add(loan);

            userDAO.update(user);

            return new UserData(user);
        } catch (PersistentException e) {
            log.error("Exception while creating loan in the database for user [id:" + userData.getId() + "]", e);
            throw new LoanServiceException(e.getMessage());
        } catch (Exception e) {
            log.error("Unexpected exception while creating loan for user [id:" + userData.getId() + "]", e);
            throw new LoanServiceException(e.getMessage());
        }
    }

    private Loan updateAndPersistLoan(Loan loan, User user) {
        loan.setOwner(user);
        loan.setApplicationDate(new Date());
        // Perform risk analysis
        LoanRisk loanRisk = loanRiskService.analyzeAndCreateRisk(loan);
        if (!loanRisk.isAcceptable()) {
            // Risk is considered too high
            loan.setLoanStatus(LoanStatus.REJECTED);
        }
        loan.setLoanRisk(loanRisk);
        Long loanId = loanDAO.create(loan);
        loan.setId(loanId);

        return loan;
    }

    @Transactional
    @Override
    public Loan getLoanById(Long loanId) {
        try {
            return loanDAO.getById(loanId);
        } catch (PersistentException e) {
            log.error("Could not find loan by id: " + loanId);
            throw new LoanServiceException(e.getMessage());
        }
    }

    @Transactional
    @Override
    public boolean payLoanForUser(Long loanId, Long loanUserId) {
        try {
            Loan loan = getLoanById(loanId);
            final User loanOwner = loan.getOwner();

            if (loanBelongsToUser(loan, loanOwner.getId())) {
                loan.setLoanStatus(LoanStatus.PAID);
                updateAndPersistLoan(loan, loanOwner);
                return true;
            }
            return false;
        } catch (PersistentException e) {
            log.error("Could not pay loan with id: " + loanId + " for user with id: " + loanUserId, e);
            return false;
        } catch (Exception e) {
            log.error("Unexpected exception while paying loan with id: " + loanId + " for user with id: " + loanUserId, e);
            throw new LoanServiceException(e.getMessage());
        }
    }

    private boolean loanBelongsToUser(Loan loan, Long loanOwnerId) {
        return LoanStatus.PAID != loan.getLoanStatus() && loan.getOwner().getId().equals(loanOwnerId);
    }

    @Transactional
    @Override
    public Loan extendLoan(Long loanId, Byte loanExtensionDays) {
        try {
            Loan loan = getLoanById(loanId);
            LoanExtension loanExtension = createLoanExtension(loan, loanExtensionDays);

            loan.setReturnDate(loanExtension.getReturnDate());
            loan.getLoanExtensions().add(loanExtension);

            loanExtensionDAO.create(loanExtension);
            loanDAO.update(loan);

            return loan;
        } catch (PersistentException e) {
            log.error("Could not extend loan with id: " + loanId, e);
            throw new LoanServiceException(e.getMessage());
        } catch (Exception e) {
            log.error("Unexpected exception while extending loan with id: " + loanId, e);
            throw new LoanServiceException(e.getMessage());
        }
    }

    private LoanExtension createLoanExtension(Loan loan, Byte loanExtensionDays) {
        final Date applicationDate = loan.getReturnDate();

        LoanExtension loanExtension = new LoanExtension();
        loanExtension.setApplicationDate(applicationDate);
        loanExtension.setReturnDate(DateUtil.addDaysToDate(applicationDate, loanExtensionDays));
        loanExtension.setLoan(loan);

        return loanExtension;
    }

}
