package com.finance.service.user.data;

import com.finance.domain.User;
import com.finance.domain.loan.Loan;
import com.finance.service.auth.AuthUtil;
import com.finance.util.CollectionsUtil;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.List;

/**
 * @author vitaly.derkach
 */
public class UserData implements UserDetails {

    private User user;

    public UserData(User user) {
        this.user = user;
    }

    @Override
    public Collection<GrantedAuthority> getAuthorities() {
        return AuthUtil.getDefaultAuthorities();
    }

    @Override
    public String getPassword() {
        return user.getPassword();
    }

    @Override
    public String getUsername() {
        return user.getUsername();
    }

    @Override
    public boolean isAccountNonExpired() {
        return user.isActive();
    }

    @Override
    public boolean isAccountNonLocked() {
        return user.isActive();
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return user.isActive();
    }

    @Override
    public boolean isEnabled() {
        return user.isActive();
    }

    public Long getId() {
        return user.getId();
    }

    public List<Loan> getLoans() {
        return user.getLoans();
    }

    public Loan getLatestLoan() {
        return CollectionsUtil.getLastElementInList(getLoans());
    }
}
