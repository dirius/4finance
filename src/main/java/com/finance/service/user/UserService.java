package com.finance.service.user;

import com.finance.domain.User;
import com.finance.service.user.data.UserData;

/**
 * @author vitaly.derkach
 */
public interface UserService {

    UserData createUser(User user);

    UserData loadUserByUsername(String username);

    UserData getById(Long userId);

}
