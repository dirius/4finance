package com.finance.service.user;

import com.finance.dao.user.UserDAO;
import com.finance.domain.User;
import com.finance.exception.PersistentException;
import com.finance.exception.ServiceException;
import com.finance.service.user.data.UserData;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service layer that performs all User-related manipulations
 *
 * @author vitaly.derkach
 */
@Service
@Qualifier("userService")
public class UserServiceImpl implements UserService {

    private static final Logger log = Logger.getLogger(UserServiceImpl.class);

    @Autowired
    private UserDAO userDAO;

    @Override
    @Transactional
    public UserData createUser(User user) {
        try {
            final Long userId = userDAO.create(user);
            if (userId != null) {
                return new UserData(user);
            }
            return null;
        } catch (PersistentException e) {
            log.error("Error creating new user", e);
            throw new ServiceException(e.getMessage());
        } catch (Exception e) {
            log.error("Unexpected error while creating user", e);
            throw new ServiceException(e.getMessage());
        }
    }

    @Override
    @Transactional
    public UserData loadUserByUsername(String username) {
        try {
            User user = userDAO.getByUsername(username);
            if (user != null) {
                return new UserData(user);
            }
            return null;
        } catch (PersistentException e) {
            log.error("Error loading user by name from db", e);
            throw new ServiceException(e.getMessage());
        } catch (Exception e) {
            log.error("Unexpected error while loading user", e);
            throw new ServiceException(e.getMessage());
        }
    }

    @Override
    @Transactional
    public UserData getById(Long userId) {
        try {
            User user = userDAO.getById(userId);
            if (user != null) {
                return new UserData(user);
            }
            return null;
        } catch (PersistentException e) {
            log.error("Error loading user by name from db", e);
            throw new ServiceException(e.getMessage());
        } catch (Exception e) {
            log.error("Unexpected error while loading user", e);
            throw new ServiceException(e.getMessage());
        }
    }
}
