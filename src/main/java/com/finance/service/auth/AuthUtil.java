package com.finance.service.auth;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.GrantedAuthorityImpl;

import java.util.ArrayList;
import java.util.List;

/**
 * @author vitaly.derkach
 */
public class AuthUtil {

    // For the purpose of simplicity of this homework application, authorities will be
    // used as default for all registered users and hardcoded as "ROLE_USER"
    private static final List<GrantedAuthority> defaultAuthorities;

    static {
        defaultAuthorities = new ArrayList<GrantedAuthority>();
        defaultAuthorities.add(new GrantedAuthorityImpl("ROLE_USER"));
    }

    public static List<GrantedAuthority> getDefaultAuthorities() {
        return defaultAuthorities;
    }
}
