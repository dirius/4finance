package com.finance.service.auth;

import com.finance.service.user.UserService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

/**
 * @author vitaly.derkach
 */
public class UserAuthService implements UserDetailsService {

    private static final Logger log = Logger.getLogger(UserAuthService.class);

    @Autowired
    private UserService userService;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException, DataAccessException {
        try {
            return userService.loadUserByUsername(username);
        } catch (Exception e) {
            log.error("Could not retrieve user by name", e);
        }
        return null;
    }
}
