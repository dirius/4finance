package com.finance.web.slider.data;

import com.finance.dao.user.UserTestUtil;
import com.finance.util.DateUtil;
import junit.framework.Assert;
import org.junit.Test;

import java.util.Date;

/**
 * @author vitaly.derkach
 */
public class LoanDataFactoryTest {

    @Test
    public void createSliderDataTest() {

        final Integer loanAmount = 100;
        final Byte loanDays = 15;

        LoanJSONData loanData = UserTestUtil.createLoanData(loanAmount, loanDays);

        Assert.assertEquals("LoanData is invalid!!!", true, loanData.isValid());

        final Date returnDate = DateUtil.addDaysCurrentTime(loanDays);

        Assert.assertEquals("Return dates are not equal", returnDate, loanData.getReturnDate());


    }

    @Test
    public void calculateCommissionTest() {
        // Take loan for 1 month with the amount of Ls 300
        // Commission must be Ls 45 - so we test it
        final Integer loanAmount = 300;
        final Byte loanDays = DateUtil.DAYS_IN_MONTH;

        LoanJSONData loanData = UserTestUtil.createLoanData(loanAmount, loanDays);

        Assert.assertEquals("Commission calculating algorithm is spoiled!", 45.0, loanData.getCommission());
    }
}
