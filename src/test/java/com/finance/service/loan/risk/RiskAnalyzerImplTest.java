package com.finance.service.loan.risk;

import com.finance.dao.user.UserTestUtil;
import com.finance.domain.User;
import com.finance.domain.loan.Loan;
import com.finance.domain.loan.LoanRisk;
import com.finance.util.DateUtil;
import com.finance.web.slider.data.LoanJSONData;
import junit.framework.Assert;
import org.junit.Test;

import java.util.Date;

/**
 * @author vitaly.derkach
 */
public class RiskAnalyzerImplTest {

    @Test
    public void testAnalyze() throws Exception {
        final RiskAnalyzer riskAnalyzer = new RiskAnalyzerImpl();

        LoanJSONData loanJSONData = UserTestUtil.createLoanData(300, (byte) 4);

        User user = UserTestUtil.createValidUserWithFilledData();

        Loan loan = new Loan();
        loan.setAmount(loanJSONData.getAmount());
        loan.setTotalAmount(loanJSONData.getTotal());
        loan.setReturnDate(loanJSONData.getReturnDate());
        loan.setApplicationDate(new Date());
        loan.setOwner(user);

        LoanRisk risk = riskAnalyzer.analyze(loan);

        // Risk must be too high
        Assert.assertEquals(false, risk.isAcceptable());

        // Now lets modify risk to bee acceptable
        loan.setReturnDate(DateUtil.addDaysCurrentTime((byte)30));

        risk = riskAnalyzer.analyze(loan);

        Assert.assertEquals(true, risk.isAcceptable());

        // Middle boundary
        loan.setAmount(170);
        loan.setTotalAmount(170.20);
        loan.setReturnDate(DateUtil.addDaysCurrentTime((byte)5));
        risk = riskAnalyzer.analyze(loan);

        Assert.assertEquals(true, risk.isAcceptable());

        loan.setAmount(20);
        loan.setTotalAmount(20.20);
        loan.setReturnDate(DateUtil.addDaysCurrentTime((byte)2));
        risk = riskAnalyzer.analyze(loan);

        Assert.assertEquals(true, risk.isAcceptable());

        loan.setAmount(20);
        loan.setTotalAmount(20.10);
        loan.setReturnDate(DateUtil.addDaysCurrentTime((byte)30));
        risk = riskAnalyzer.analyze(loan);

        Assert.assertEquals(true, risk.isAcceptable());
    }
}
