package com.finance.service.loan;

import com.finance.dao.user.UserTestUtil;
import com.finance.domain.User;
import com.finance.domain.loan.Loan;
import com.finance.domain.loan.LoanExtension;
import com.finance.domain.loan.LoanStatus;
import com.finance.exception.ServiceException;
import com.finance.service.user.UserService;
import com.finance.service.user.data.UserData;
import com.finance.util.DateUtil;
import com.finance.web.slider.data.LoanJSONData;
import java.util.Date;
import junit.framework.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author vitaly.derkach
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("/webapp/conf/spring/test/testContext.xml")
@Transactional
public class LoanServiceImplTest {

    @Autowired
    private UserService userService;

    @Autowired
    private LoanService loanService;

    @Test
    public void testCreateLoanForUser() throws Exception {

        final User user = UserTestUtil.createValidUserWithFilledData();

        //Create user
        UserData userData = userService.createUser(user);
        LoanJSONData loanJSONData = UserTestUtil.createLoanData(UserTestUtil.LOAN_AMOUNT, (byte) DateUtil.DAYS_IN_MONTH);

        try {
            loanService.createLoanForUser(userData, loanJSONData.getSimpleLoan());
            UserData updatedData = userService.getById(userData.getId());

            Assert.assertEquals("Loan was not inserted for user.", 1, updatedData.getLoans().size());

        } catch (ServiceException e) {
            Assert.fail("Loan service exception occurred while creating a new loan: " + e.getMessage());
        }

    }

    @Test
    public void testPayLoan() {
        final User user = UserTestUtil.createValidUserWithFilledData();
        userService.createUser(user);

        Loan loan = UserTestUtil.createLoanForUser(user);
        loanService.createLoanForUser(new UserData(user), loan);

        final boolean wasSuccessful = loanService.payLoanForUser(loan.getId(), user.getId());

        Assert.assertTrue(wasSuccessful);

        Loan paidLoan = loanService.getLoanById(loan.getId());

        Assert.assertEquals(paidLoan.getLoanStatus(), LoanStatus.PAID);
    }

    @Test
    public void testExtendLoan() {
        final User user = UserTestUtil.createValidUserWithFilledData();
        userService.createUser(user);

        Loan loan = UserTestUtil.createLoanForUser(user);
        Date returnDate = loan.getReturnDate();
        loanService.createLoanForUser(new UserData(user), loan);

        Loan extendedLoan = loanService.extendLoan(loan.getId(), (byte) 7);

        Assert.assertNotNull(extendedLoan);

        Assert.assertEquals(DateUtil.addDaysToDate(returnDate, (byte) 7), extendedLoan.getReturnDate());



    }

}
