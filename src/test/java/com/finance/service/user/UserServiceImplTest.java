package com.finance.service.user;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author vitaly.derkach
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("/webapp/conf/spring/test/testContext.xml")
@Transactional
public class UserServiceImplTest {

    @Autowired
    private UserService userService;

    @Test
    public void testCreateUser() throws Exception {

    }

    @Test
    public void testLoadUserByUsername() throws Exception {

    }
}
