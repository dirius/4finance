package com.finance.dao.user;

import com.finance.domain.User;
import junit.framework.Assert;

import org.junit.Test;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.Set;

/**
 * @author vitaly.derkach
 */
public class UserValidatorTest {

    @Test
    public void testUserValidation() {
        User user = UserTestUtil.createValidUserWithFilledData();

        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        Validator validator = factory.getValidator();


        // Check user
        Assert.assertNotNull(user);

        // Valid
        Set<ConstraintViolation<User>> constraintViolations =  validator.validate(user);
        Assert.assertEquals(0, constraintViolations.size());

        // Validate password
        user.setPassword("");
        constraintViolations = validator.validate(user);
        Assert.assertEquals(1, constraintViolations.size());

        // Validate email
        user.setEmail("aax");
        constraintViolations = validator.validate(user);
        Assert.assertEquals(2, constraintViolations.size());

    }

}
