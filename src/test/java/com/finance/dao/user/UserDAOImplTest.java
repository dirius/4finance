package com.finance.dao.user;

import com.finance.domain.User;
import junit.framework.Assert;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author vitaly.derkach
 */
public class UserDAOImplTest {

    @Autowired
    private UserDAO userDAO;

    public void testCreate() {
        User user = new User();
        user.setUsername("Jack");
        user.setPassword("123");
        Long id = userDAO.create(user);
        Assert.assertNotNull(id);
    }
}
