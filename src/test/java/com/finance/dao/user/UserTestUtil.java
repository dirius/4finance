package com.finance.dao.user;

import com.finance.domain.User;
import com.finance.domain.loan.Loan;
import com.finance.util.DateUtil;
import com.finance.web.slider.data.LoanJSONData;
import com.finance.web.slider.data.LoanDataFactory;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author vitaly.derkach
 */
public class UserTestUtil {

    public static final Integer LOAN_AMOUNT = 100;

    public static User createValidUserWithFilledData() {
        User user = new User();
        user.setUsername("asdasd");
        user.setPassword("asdasd");
        user.setEmail("asd@asd.com");
        user.setGender((byte) 1);
        user.setId(1L);
        return user;
    }

    public static LoanJSONData createLoanData(Integer loanAmount, Byte loanDays) {
        return LoanDataFactory.createLoanDataFromRequestMap(getJsonMap(loanAmount, loanDays));
    }

    private static Map<String, String> getJsonMap(Integer loanAmount, Byte loadDays) {
        Map<String, String> jsonFromRequest = new HashMap<String, String>();
        jsonFromRequest.put(LoanDataFactory.PARAM_AMOUNT, loanAmount.toString());
        jsonFromRequest.put(LoanDataFactory.PARAM_TIME, loadDays.toString());
        return jsonFromRequest;
    }

    public static Loan createLoanForUser(User user) {
        Loan loan = new Loan();
        loan.setOwner(user);
        loan.setAmount(200);
        loan.setTotalAmount(200.20);
        loan.setReturnDate(DateUtil.addDaysCurrentTime((byte) 14));
        loan.setApplicationDate(new Date());

        return loan;
    }
}
