package com.finance.dao;

/**
 * @author vitaly.derkach
 */
public interface CommonDAOTest {

    void removeTest();

    void createAndGetTest();

    void updateTest();

}
