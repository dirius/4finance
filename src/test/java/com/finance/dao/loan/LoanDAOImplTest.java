package com.finance.dao.loan;

import com.finance.domain.loan.Loan;
import com.finance.util.DateUtil;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

/**
 * @author vitaly.derkach
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("/webapp/conf/spring/test/testContext.xml")
@Transactional
public class LoanDAOImplTest {

    @Autowired
    private LoanDAO loanDAO;

    @Test
    public void removeTest() {
        //

    }

    @Test
    public void createAndGetTest() {
        // Init test data
        final Date afterOneWeek = DateUtil.addDaysCurrentTime((byte) DateUtil.DAYS_IN_WEEK);
        final Double initialLoanAmount = 100.00;

        // Create entity
        Loan loan = new Loan();
        loan.setApplicationDate(new Date());
        loan.setReturnDate(afterOneWeek);
        loan.setTotalAmount(initialLoanAmount);

        // Test
        final Long loanId = loanDAO.create(loan);
        Loan createdLoan = loanDAO.getById(loanId);
        Assert.assertNotNull(createdLoan);

        Double createdLoanAmount = createdLoan.getTotalAmount();
        Assert.assertEquals("Initial loan amount and after creation loan amount does not match!", initialLoanAmount, createdLoanAmount);

        //Assert.assertEquals("Loan extensions size is not 4 as expected", 4, createdLoan.getLoanExtensions().size());


    }

}
